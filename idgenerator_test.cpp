/**
    File: idgenerator_Test.cpp
    
    Модульный тест класса для генерирования идентификаторов.
*/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE idGeneratorTest
#include <boost/test/unit_test.hpp>

#include "idgenerator.hpp"


BOOST_AUTO_TEST_SUITE(idGeneratorTestSuite)


struct TestObject
{ 
  TestObject(): generator()
  {}
  
  IdGenerator<size_t> generator;
};


BOOST_FIXTURE_TEST_CASE(getId, TestObject)
{	
	for (size_t n = 1; n <= 10; ++n)
	{
		BOOST_CHECK_EQUAL(n, generator.getId());
	}
}


BOOST_FIXTURE_TEST_CASE(overflovCounter, TestObject)
{
  generator.generatorCounter_ = std::numeric_limits<size_t>::max();  
  
  BOOST_CHECK_THROW(generator.getId(), std::exception);
}


BOOST_FIXTURE_TEST_CASE(reset, TestObject)
{
  BOOST_CHECK_EQUAL(1, generator.getId());
  BOOST_CHECK_EQUAL(2, generator.getId());
  BOOST_CHECK_EQUAL(3, generator.getId());
  BOOST_CHECK_EQUAL(4, generator.getId());  
  
  generator.reset();  
  
  BOOST_CHECK_EQUAL(1, generator.getId());
  BOOST_CHECK_EQUAL(2, generator.getId());
}


struct TestObjectAtomic
{ 
  TestObjectAtomic(): generator()
  {}
  
  IdGenerator<size_t, AtomicTraits<size_t>> generator;
};


BOOST_FIXTURE_TEST_CASE(getId_Atomic, TestObjectAtomic)
{	
	for (size_t n = 1; n <= 10; ++n)
	{
		BOOST_CHECK_EQUAL(n, generator.getId());
	}
}


BOOST_AUTO_TEST_SUITE_END() 

