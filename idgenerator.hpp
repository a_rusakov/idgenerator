/**
    File: idgenerator.hpp
    
    Реализация класса для генерирования идентификаторов.
    Идентификаторы представляют собой последовательные челые числа.
    
*/

#ifndef IDGENERATOR_HPP
#define IDGENERATOR_HPP

#include <limits>
#include <atomic>
#include <stdexcept>


/** Используется для обеспечения доступа к частным членам классов, модульными тестами.
    (Механизм boost/test/unit_test).   
*/
namespace idGeneratorTestSuite
{
	struct overflovCounter;
};


/** Свойства, обеспечивающие несколько реализаций генератора.
    Потокобезопасная реализация AtomicTraits, основана на использовании атомарных примитивных типов.
    Реализация ValueTraits, обеспечивает идеому С++ - "Не платим за то, что не используем".    
*/
template <typename T>
struct ValueTraits 
{
    using ValueType = T;
};

template <typename T>
struct AtomicTraits 
{
    using ValueType = std::atomic<T>;
};


/** Генератор идентификаторов.
*/
template <typename T, typename Traits = ValueTraits<T>>
class IdGenerator
{
  using ValueType = typename Traits::ValueType;
  static_assert(std::is_integral<T>::value, "T should be integral type!");

  public:
  
    IdGenerator()
    {
      this->reset();
    }
    
    /** Возвращает уникальное значение идентификатора.
        В случае перепонения внутреннего счетсика вызывает исключение std::exception.
    */
    auto getId() -> T
    {
      if(generatorCounter_ == std::numeric_limits<T>::max())
        throw std::exception();
      
      return ++generatorCounter_;
    }
    
    /** Сбрасывает состояние генератора.
        Метод предназначен для используется "клиентсим" кодом, после получения
        исключения на вызов метода getId() или если этого требует логика приложения
        используюего генератор.  
    */    
    void reset()
    {
      generatorCounter_ = 0;
    }

  private:
  
    // Внутренний счётчик уникального идентификатора.
    ValueType generatorCounter_;


  // Обеспечивает доступ к частным членам классов, модульными тестами.  
  friend struct ::idGeneratorTestSuite::overflovCounter;

};

#endif//IDGENERATOR_HPP

