
CXX           = g++
CXXFLAGS      = -pipe -O2 -Wall -W -std=c++11
INCPATH       = -I.
LINK          = g++
LFLAGS        = -Wl,-O1
LIBS          = -lboost_unit_test_framework
DEL_FILE      = rm -f

SOURCES       = idgenerator_test.cpp 
OBJECTS       = idgenerator_test.o 
TARGET        = idgenerator_test

all: $(TARGET)

$(TARGET):  $(OBJECTS)  
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)

idgenerator_test.o: idgenerator_test.cpp idgenerator.hpp
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o idgenerator_test.o idgenerator_test.cpp


runtest: $(TARGET)
	./$(TARGET)
    

clean:
	-$(DEL_FILE) $(OBJECTS)

distclean: clean 
	-$(DEL_FILE) $(TARGET) 

cleanall: clean distclean
	
